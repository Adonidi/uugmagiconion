﻿using Grpc.Core;
using MagicOnion.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GamingHubClient : IGamingHubReceiver
{
  Dictionary<string, GameObject> players = new Dictionary<string, GameObject>();

  IGamingHub client;

  public async Task<GameObject> ConnectAsync( Channel grpcChannel, string roomName, string playerName )
  {
    client = StreamingHubClient.Connect<IGamingHub, IGamingHubReceiver>( grpcChannel, this );

    var roomPlayers = await client.JoinAsync( roomName, playerName, 0.0f, 0.0f, 0.0f );
    foreach ( var player in roomPlayers )
    {
      (this as IGamingHubReceiver).OnJoin( player );
    }

    return players[playerName];
  }

  // methods send to server.

  public Task LeaveAsync()
  {
    return client.LeaveAsync();
  }

  public Task MoveAsync( Vector3 position )
  {
    return client.MoveAsync( position.x, position.y, position.z );
  }

  // dispose client-connection before channel.ShutDownAsync is important!
  public Task DisposeAsync()
  {
    return client.DisposeAsync();
  }

  // You can watch connection state, use this for retry etc.
  public Task WaitForDisconnect()
  {
    return client.WaitForDisconnect();
  }

  // Receivers of message from server.

  void IGamingHubReceiver.OnJoin( Player player )
  {
    Debug.Log( "Join Player:" + player.Name );

    if ( players.TryGetValue( player.Name, out GameObject old ) )
      GameObject.Destroy( old );

    var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
    cube.name = player.Name;
    cube.transform.localPosition = new Vector3( player.x, player.y, player.z );
    players[player.Name] = cube;
  }

  void IGamingHubReceiver.OnLeave( Player player )
  {
    Debug.Log( "Leave Player:" + player.Name );

    if ( players.TryGetValue( player.Name, out var cube ) )
    {
      GameObject.Destroy( cube );
    }
  }

  void IGamingHubReceiver.OnMove( Player player )
  {
    Debug.Log( "Move Player:" + player.Name );

    if ( players.TryGetValue( player.Name, out var cube ) )
    {
      cube.transform.localPosition = new Vector3( player.x, player.y, player.z );
    }
  }
}