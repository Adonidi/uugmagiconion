﻿using Grpc.Core;
using MagicOnion.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;


public class GameController : MonoBehaviour
{
  // standard gRPC channel
  private Channel channel = new Channel("localhost", 12345, ChannelCredentials.Insecure);


  // Start is called before the first frame update
  private async void Start()
  {
    //await gamingHub();
    await services();
  }

  #region Gaming Hub
  private GamingHubClient ghc;

  private async Task gamingHub()
  {
    ghc = new GamingHubClient();
    GameObject go = await ghc.ConnectAsync( channel, "playroom", System.DateTime.Now.ToString() );

    Coroutine track_cor = StartCoroutine( trackObject( go ) );
  }

  private IEnumerator trackObject( GameObject go )
  {
    Vector3 last_pos = go.transform.localPosition;
    while( true )
    {
      yield return null;

      if ( Vector2.Distance( last_pos, go.transform.localPosition ) > 0.01f )
        ghc.MoveAsync( go.transform.localPosition );
      last_pos = go.transform.localPosition;
    }
  }

  private async void OnApplicationQuit()
  {
    if ( ghc == null )
      return;
    await ghc.LeaveAsync();
    await ghc.DisposeAsync();
  }
  #endregion Gaming Hub

  #region Services
  private async Task services()
  {
    // get MagicOnion dynamic client proxy
    IMyFirstService client = MagicOnionClient.Create<IMyFirstService>(
        channel
        , new IClientFilter[]
        {
            new LoggingFilter()
          , new RetryFilter()
        }
    );

    // call method.
    for ( int i = 0; i < 100; i++ )
    {
      client.SumAsync( 0, i );
    }

    string res = await client.getMyString( "asdqw ", 234 );

    //Debug.Log( "Client Received:" + result + " " + res );
  }
  #endregion Services
}
