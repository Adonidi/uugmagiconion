﻿using MagicOnion.Client;
using System;
using System.Diagnostics;
using System.Threading.Tasks;


public class AppendHeaderFilter : IClientFilter
{
  public async ValueTask<ResponseContext> SendAsync( RequestContext context, Func<RequestContext, ValueTask<ResponseContext>> next )
  {
    // add the common header(like authentcation).
    var header = context.CallOptions.Headers;
    header.Add( "x-foo", "abcdefg" );
    header.Add( "x-bar", "hijklmn" );
    header.Add( "user_id", "213142" );

    return await next( context );
  }
}

public class LoggingFilter : IClientFilter
{
  public async ValueTask<ResponseContext> SendAsync( RequestContext context, Func<RequestContext, ValueTask<ResponseContext>> next )
  {
    UnityEngine.Debug.Log( "Request Begin:" + context.MethodPath ); // Debug.Log in Unity.

    var sw = Stopwatch.StartNew();
    var response = await next(context);
    sw.Stop();

    UnityEngine.Debug.Log( "Request Completed:" + context.MethodPath + ", Elapsed:" + sw.Elapsed.TotalMilliseconds + "ms" );

    return response;
  }
}

public class RetryFilter : IClientFilter
{
  public async ValueTask<ResponseContext> SendAsync( RequestContext context, Func<RequestContext, ValueTask<ResponseContext>> next )
  {
    Exception lastException = null;
    var retryCount = 0;
    while ( retryCount != 3 )
    {
      try
      {
        // using same CallOptions so be careful to add duplicate headers or etc.
        return await next( context );
      } catch ( Exception ex )
      {
        lastException = ex;
      }
      retryCount++;
    }

    throw new Exception( "Retry failed", lastException );
  }
}
