﻿using MessagePack;
using System.Numerics;


// for example, request object by MessagePack.
[MessagePackObject]
public class Player
{
  [Key( 0 )]
  public string Name { get; set; }
  [Key( 1 )]
  public float x;
  [Key( 2 )]
  public float y;
  [Key( 3 )]
  public float z;
}