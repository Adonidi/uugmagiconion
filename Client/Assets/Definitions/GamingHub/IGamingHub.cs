﻿using MagicOnion;
using System.Threading.Tasks;


// Client -> Server definition
// implements `IStreamingHub<TSelf, TReceiver>`  and share this type between server and client.
public interface IGamingHub : IStreamingHub<IGamingHub, IGamingHubReceiver>
{
  // return type shuold be `Task` or `Task<T>`, parameters are free.
  Task<Player[]> JoinAsync( string roomName, string userName, float x, float y, float z );
  Task LeaveAsync();
  Task MoveAsync( float x, float y, float z );
}
