﻿using MagicOnion.Server.Hubs;
using System;
using System.Threading.Tasks;


public class MyStreamingHubFilterAttribute : StreamingHubFilterAttribute
{
  public override async ValueTask Invoke( StreamingHubContext context, Func<StreamingHubContext, ValueTask> next )
  {
    Console.WriteLine( $"Before call" );
    await next( context );
    Console.WriteLine( $"After call" );
  }
}