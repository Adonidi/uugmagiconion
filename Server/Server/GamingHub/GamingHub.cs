﻿using MagicOnion.Server.Hubs;
using System.Linq;
using System.Threading.Tasks;


// Server implementation
// implements : StreamingHubBase<THub, TReceiver>, THub

public class GamingHub : StreamingHubBase<IGamingHub, IGamingHubReceiver>, IGamingHub
{
  // this class is instantiated per connected so fields are cache area of connection.
  private IGroup room;
  private Player self;
  private IInMemoryStorage<Player> storage;


  [MyStreamingHubFilter]
  public async Task<Player[]> JoinAsync( string roomName, string userName, float x, float y, float z )
  {
    self = new Player() { Name = userName, x = x, y = y, z = z };

    // Group can bundle many connections and it has inmemory-storage so add any type per group. 
    (room, storage) = await Group.AddAsync( roomName, self );

    // Typed Server->Client broadcast.
    Broadcast( room ).OnJoin( self );

    return storage.AllValues.ToArray();
  }

  public async Task LeaveAsync()
  {
    await room.RemoveAsync( this.Context );
    Broadcast( room ).OnLeave( self );
  }

  public async Task MoveAsync( float x, float y, float z )
  {
    self.x = x;
    self.y = y;
    self.z = z;

    BroadcastExceptSelf( room ).OnMove( self );
  }

  // You can hook OnConnecting/OnDisconnected by override.
  protected override async ValueTask OnDisconnected()
  {
    // on disconnecting, if automatically removed this connection from group.
    //return CompletedTask;
  }
}