﻿using Grpc.Core;
using MagicOnion.Server;
using System;


namespace Server
{
  class Program
  {
    static void Main( string[] args )
    {
      GrpcEnvironment.SetLogger( new Grpc.Core.Logging.ConsoleLogger() );

      // setup MagicOnion and option.
      MagicOnionServiceDefinition service = MagicOnionEngine.BuildServerServiceDefinition(isReturnExceptionStackTraceInErrorDetail: true);

      Grpc.Core.Server server = new global::Grpc.Core.Server
      {
        Services = { service },
        Ports = { new ServerPort("localhost", 12345, ServerCredentials.Insecure) }
      };

      // launch gRPC Server.
      server.Start();

      // and wait.
      Console.ReadLine();
    }
  }
}
