﻿using MagicOnion;
using MagicOnion.Server;


// implement RPC service to Server Project.
// inehrit ServiceBase<interface>, interface
[SampleFilter]
[SampleFilter]
[SampleFilter]
public class MyFirstService : ServiceBase<IMyFirstService>, IMyFirstService
{
  [SampleFilter]
  public async UnaryResult<string> getMyString( string a, int b )
  {
    return a + b;
  }

  // You can use async syntax directly.
  public async UnaryResult<int> SumAsync( int x, int y )
  {
    object obj;
    Context.Items.TryGetValue( "user", out obj );
    Player player = (Player)obj;

    Logger.Debug( $"Received:{x}, {y}" );

    return x + y;
  }
}