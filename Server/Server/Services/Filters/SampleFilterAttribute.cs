﻿using MagicOnion.Server;
using MagicOnion;
using System;
using System.Threading.Tasks;


// You can attach per class/method like [SampleFilter]
// for StreamingHub methods, implement StreamingHubFilterAttribute instead.
public class SampleFilterAttribute : MagicOnionFilterAttribute
{
  public override async ValueTask Invoke( ServiceContext context, Func<ServiceContext, ValueTask> next )
  {
    try
    {
      string user_id = context.CallContext.RequestHeaders.GetValue( "user_id" );
      context.Items.TryAdd( "user", new Player() );

      Console.WriteLine( $"user {user_id} is calling!" );

      /* on before */
      await next( context ); // next
      /* on after */
    } catch
    {
      /* on exception */
      throw;
    } finally
    {
      /* on finally */
    }
  }
}